Team Name: Solo
Project Title: Vending Machine
Team Members: Rahul Majmudar (This is a team of one)

Project Description:
Design a vending machine that dispenses $0.5 and $1 snacks (outputs).
The vending machine accepts dimes, quarters, and $1 bills for payment (inputs).
There will be a dispense50 and dispense100 button (inputs) to dispense the product.
The machine will not accept any amount above $1 and doesn't dispense any change.
The design will be in the form of a state machine. Once the design is completed, it will be coded and
tested on a STM32 board. 7 segment led outputs on a bread board will be used as well. Possibly change 
will be displayed on a third 7 segment led.
